﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabMgr : MonoBehaviour
{
    public OVRGrabbable ovrGrabable;
    public GameObject descObj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ovrGrabable.isGrabbed)
        {
            descObj.SetActive(true);
        }
        else
        {
            descObj.SetActive(false);
        }
    }
}
