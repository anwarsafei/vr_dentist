﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour
{
    public GameObject canvasIntro;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitIntro());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator waitIntro()
    {
        canvasIntro.SetActive(true);
        yield return new WaitForSeconds(57);
        canvasIntro.SetActive(false);
    }
}
