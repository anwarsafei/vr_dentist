﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMgr : MonoBehaviour
{
    public GrabMgr[] grabMgr;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PengenalanAlat(bool value)
    {
        if (value == true)
        {
            for (int i = 0; i < grabMgr.Length; i++)
            {
                grabMgr[i].enabled = true;
            }
        }

        if (value == false)
        {
            for (int i = 0; i < grabMgr.Length; i++)
            {
                grabMgr[i].enabled = false;
            }
        }
    }
}
