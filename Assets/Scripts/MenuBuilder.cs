﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEngine.SceneManagement;
#endif
public class MenuBuilder : MonoBehaviour
{
    // room for extension:
    // support update funcs
    // fix bug where it seems to appear at a random offset
    // support remove

    // Convenience consts for clarity when using multiple debug panes. 
    // But note that you can an arbitrary number of panes if you add them in the inspector.
    public const int DEBUG_PANE_CENTER = 0;
    public const int DEBUG_PANE_RIGHT = 1;
    public const int DEBUG_PANE_LEFT = 2;

    [SerializeField]
    private GameObject uiHelpersToInstantiate;

    [SerializeField]
    private Transform[] targetContentPanels;

    [SerializeField]
    private bool manuallyResizeContentPanels;

    private bool[] reEnable;

    [SerializeField]
    private List<GameObject> toEnable;
    [SerializeField]
    private List<GameObject> toDisable;

    public static MenuBuilder instance;

    public delegate void OnClick();
    public delegate void OnToggleValueChange(Toggle t);
    public delegate void OnSlider(float f);
    public delegate bool ActiveUpdate();

    private const float elementSpacing = 16.0f;
    private const float marginH = 16.0f;
    private const float marginV = 16.0f;
    private Vector2[] insertPositions;
    private List<RectTransform>[] insertedElements;
    private Vector3 menuOffset;
    OVRCameraRig rig;
    LaserPointer lp;
    LineRenderer lr;

    private Vector3 initRot;

    public LaserPointer.LaserBeamBehavior laserBeamBehavior;

    public void Awake()
    {
        //Debug.Assert(instance == null);
        instance = this;
        menuOffset = transform.localPosition;
        rig = FindObjectOfType<OVRCameraRig>();
        for (int i = 0; i < toEnable.Count; ++i)
        {
            toEnable[i].SetActive(false);
        }

        insertPositions = new Vector2[targetContentPanels.Length];
        for (int i = 0; i < insertPositions.Length; ++i)
        {
            insertPositions[i].x = marginH;
            insertPositions[i].y = -marginV;
        }
        insertedElements = new List<RectTransform>[targetContentPanels.Length];
        for (int i = 0; i < insertedElements.Length; ++i)
        {
            insertedElements[i] = new List<RectTransform>();
        }

        //GameObject UIHelpers = GameObject.FindGameObjectWithTag("UIHelper");
        //if (UIHelpers == null)
        if (uiHelpersToInstantiate)
        {
            GameObject.Instantiate(uiHelpersToInstantiate);
        }

        lp = FindObjectOfType<LaserPointer>();
        if (!lp)
        {
            Debug.LogError("Debug UI requires use of a LaserPointer and will not function without it. Add one to your scene, or assign the UIHelpers prefab to the DebugUIBuilder in the inspector.");
            return;
        }
        lp.laserBeamBehavior = laserBeamBehavior;

        if (!toEnable.Contains(lp.gameObject))
        {
            toEnable.Add(lp.gameObject);
        }
        GetComponent<OVRRaycaster>().pointer = lp.gameObject;
        //lp.gameObject.SetActive(false);


        initRot = this.transform.localEulerAngles;
    }
    [ContextMenu("Show")]
    public void Show()
    {
        Relayout();
        gameObject.SetActive(true);
        //Vector3 newEulerRot = rig.transform.rotation.eulerAngles;
        //newEulerRot.z = 0.0f;
        //newEulerRot.x = initRot.x;

        //transform.eulerAngles = newEulerRot;

        if (reEnable == null || reEnable.Length < toDisable.Count) reEnable = new bool[toDisable.Count];
        reEnable.Initialize();
        int len = toDisable.Count;
        for (int i = 0; i < len; ++i)
        {
            if (toDisable[i])
            {
                reEnable[i] = toDisable[i].activeSelf;
                toDisable[i].SetActive(false);
            }
        }
        len = toEnable.Count;
        for (int i = 0; i < len; ++i)
        {
            toEnable[i].SetActive(true);
        }

        int numPanels = targetContentPanels.Length;
        for (int i = 0; i < numPanels; ++i)
        {
            targetContentPanels[i].gameObject.SetActive(insertedElements[i].Count > 0);
        }
    }
    [ContextMenu("Hide")]
    public void Hide()
    {
        gameObject.SetActive(false);

        for (int i = 0; i < reEnable.Length; ++i)
        {
            if (toDisable[i] && reEnable[i])
            {
                toDisable[i].SetActive(true);
            }
        }

        int len = toEnable.Count;
        for (int i = 0; i < len; ++i)
        {
            toEnable[i].SetActive(false);
        }
    }

    // Currently a slow brute-force method that lays out every element. 
    // As this is intended as a debug UI, it might be fine, but there are many simple optimizations we can make.
    private void Relayout()
    {
        for (int panelIdx = 0; panelIdx < targetContentPanels.Length; ++panelIdx)
        {
            RectTransform canvasRect = targetContentPanels[panelIdx].GetComponent<RectTransform>();
            List<RectTransform> elems = insertedElements[panelIdx];
            int elemCount = elems.Count;
            float x = marginH;
            float y = -marginV;
            float maxWidth = 0.0f;
            for (int elemIdx = 0; elemIdx < elemCount; ++elemIdx)
            {
                RectTransform r = elems[elemIdx];
                r.anchoredPosition = new Vector2(x, y);
                y -= (r.rect.height + elementSpacing);
                maxWidth = Mathf.Max(r.rect.width + 2 * marginH, maxWidth);
            }
            canvasRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, maxWidth);
            canvasRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, -y + marginV);
        }
    }

    private void AddRect(RectTransform r, int targetCanvas)
    {
        if (targetCanvas > targetContentPanels.Length)
        {
            Debug.LogError("Attempted to add debug panel to canvas " + targetCanvas + ", but only " + targetContentPanels.Length + " panels were provided. Fix in the inspector or pass a lower value for target canvas.");
            return;
        }

        r.transform.SetParent(targetContentPanels[targetCanvas], false);
        insertedElements[targetCanvas].Add(r);
        if (gameObject.activeInHierarchy)
        {
            Relayout();
        }
    }

    public void ToggleLaserPointer(bool isOn)
    {
        if (lp)
        {
            if (isOn)
            {
                lp.enabled = true;
            }
            else
            {
                lp.enabled = false;
            }
        }
    }
}
